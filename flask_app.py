from membership.web.base_app import app

if __name__ == '__main__':
    app.run(host='0.0.0.0', threaded=True, port=8080)
