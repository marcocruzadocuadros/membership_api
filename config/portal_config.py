from config.lib import from_env

PORTAL_URL: str = from_env.get_str('PORTAL_URL', 'http://localhost:3000')
