import logging
import random
import dateutil.parser
from typing import Optional, Union

from membership.database.base import Session
from membership.database.models import Attendee, Meeting
from membership.services.errors import ValidationError

logger = logging.getLogger(__name__)


class MeetingService:

    class ErrorCodes:
        NAME_TAKEN = "NAME_TAKEN"
        ALREADY_ATTENDED = "ALREADY_ATTENDED"
        INVALID_CODE = "INVALID_CODE"

    def attend_meeting(self, member_id: int, short_id: int, session: Session) -> Optional[Meeting]:
        meeting: Meeting = session.query(Meeting).filter_by(short_id=short_id).one_or_none()
        if not meeting:
            return None
        if len(session.query(Attendee).filter_by(meeting_id=meeting.id,
                                                 member_id=member_id).all()) > 0:
            raise ValidationError(
                MeetingService.ErrorCodes.ALREADY_ATTENDED,
                "Member has already attended meeting {}".format(short_id)
            )
        a = Attendee()
        a.meeting = meeting
        a.member_id = member_id
        session.add(a)
        session.commit()
        return meeting

    def add_meeting(self, name: str, committee_id: Optional[int], session: Session) -> Meeting:
        if session.query(Meeting).filter_by(name=name).count() > 0:
            raise ValidationError(
                MeetingService.ErrorCodes.NAME_TAKEN,
                'A meeting with this name already exists.'
            )

        meeting = Meeting(name=name, committee_id=committee_id)
        session.add(meeting)
        return meeting

    def find_meeting_by_id(self, meeting_id: str, session: Session) -> Optional[Meeting]:
        return session.query(Meeting).get(meeting_id)

    def set_meeting_fields(self, meeting: Meeting, json: dict, session: Session) -> Meeting:
        meeting.committee_id = json.get('committee_id')
        meeting.landing_url = json.get('landing_url')

        if 'code' in json:
            meeting = self.set_meeting_code(meeting, json['code'], session)

        if 'start_time' in json:
            if json['start_time'] is None:
                meeting.start_time = None
            else:
                meeting.start_time = dateutil.parser.parse(json['start_time'])
        if 'end_time' in json:
            if json['end_time'] is None:
                meeting.end_time = None
            else:
                meeting.end_time = dateutil.parser.parse(json['end_time'])

        session.add(meeting)
        session.commit()
        return meeting

    def set_meeting_code(self,
                         meeting: Meeting,
                         code: Union[int, str],
                         session: Session) -> Meeting:
        if code == 'autogenerate':
            if meeting.short_id:
                logging.info(
                    'Meeting id={} already has meeting code={}'.format(meeting.id, meeting.short_id)
                )
                short_id = None
            else:
                short_id = self.generate_meeting_code(session)
        else:
            try:
                short_id = int(code)
            except (TypeError, ValueError):
                raise ValidationError(
                    MeetingService.ErrorCodes.INVALID_CODE,
                    'Invalid "code". Expected 4 digit integer or "autogenerate".'
                )

        if short_id is not None:
            if short_id < 1000 or short_id > 9999:
                raise ValidationError(
                    MeetingService.ErrorCodes.INVALID_CODE,
                    'Invalid "code". Code should be 4 digits long.'
                )
            meeting.short_id = short_id

        return meeting

    def generate_meeting_code(self, session: Session) -> int:
        for _ in range(5):
            short_id = random.randint(1000, 9999)
            if session.query(Meeting).filter_by(short_id=short_id).count() == 0:
                return short_id

        raise Exception('Failed to find an unused meeting code after five tries.')
