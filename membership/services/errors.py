from typing import Optional


class ValidationError(Exception):
    def __init__(self, key: str, message: str, cause: Optional[Exception] = None):
        super().__init__(message, cause)
        self.key = key
        self.message = message
        self.cause = cause
