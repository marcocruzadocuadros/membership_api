from typing import List, Tuple

from membership.database.base import Session
from membership.database.models import Member, Meeting
from membership.models import EligibleMember
from membership.repos import MeetingRepo


class AttendeeService:

    def __init__(self, meetings: MeetingRepo) -> None:
        self.meetings = meetings

    def members_as_eligible(self, session: Session, members: List[Member]) -> List[EligibleMember]:
        recent_meetings = self.meetings.most_recent(session)

        def member_as_eligible(member: Member) -> EligibleMember:
            meetings = [a.meeting for a in member.meetings_attended]
            (is_eligible, message) = self._eligibility_details(meetings, recent_meetings)
            return EligibleMember(
                member,
                is_eligible=is_eligible,
                message=message,
            )

        return [member_as_eligible(m) for m in members]

    @staticmethod
    def _eligibility_details(attended_meetings: List[Meeting],
                             recent_meetings: List[Meeting]) -> Tuple[bool, str]:
        """
        True if the member attended the last two out of three meetings,
        False otherwise
        """
        recent_meeting_ids = {m.id for m in recent_meetings}
        recent_meeting_attendance = [
            m for m in attended_meetings if m.id in recent_meeting_ids
        ]
        is_eligible = len(recent_meeting_attendance) >= 2
        message = 'eligible' if is_eligible else 'not eligible'
        if recent_meeting_attendance:
            message += ' ({})'.format(
                ', '.join(m.start_time.strftime('%b') for m in recent_meeting_attendance)
            )
        return (
            is_eligible,
            message,
        )
