from typing import List, Type

from sqlalchemy import and_
from membership.database.base import Session
from membership.database.models import Meeting


class MeetingRepo:

    def __init__(self, meeting_table: Type[Meeting]) -> None:
        self.meeting_table = meeting_table

    def most_recent(self, session: Session, limit: int = 3) -> List[Meeting]:
        query = session \
                .query(self.meeting_table) \
                .filter(and_(
                    self.meeting_table.start_time != None,
                    self.meeting_table.end_time != None,
                )) \
                .order_by(
                    self.meeting_table.start_time.asc(),
                )[-limit:]

        return list(query)
