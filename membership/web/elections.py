from flask import Blueprint, jsonify, request

from membership.database.base import Session
from membership.database.models import Candidate, Election, Member, EligibleVoter, Vote, Ranking
from membership.models import AuthContext
from membership.services import ElectionService
from membership.util.vote import STVElection
from membership.web.auth import requires_auth
from membership.web.util import BadRequest, NotFound, ServerError
from membership.web.submit_vote_handler import handle_vote, VotingError

from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import joinedload

import logging
import random

election_api = Blueprint('election_api', __name__)
election_service = ElectionService()


@election_api.route('/election/list', methods=['GET'])
@requires_auth(admin=False)
def get_elections(ctx: AuthContext):
    elections = election_service.list_elections(ctx.session)
    result = {e.id: e.name for e in elections}
    return jsonify(result)


@election_api.route('/election', methods=['GET'])
@requires_auth(admin=False)
def get_election_by_id(ctx: AuthContext):
    election_id = request.args['id']
    election = election_service.find_election_by_id(ctx.session, election_id)
    if election is None:
        return NotFound('Election id={} does not exist'.format(election_id))
    else:
        result = {
            'name': election.name,
            'number_winners': election.number_winners,
            'candidates': [
                {
                    'id': candidate.id,
                    'name': candidate.name,
                    'image_url': candidate.image_url,
                } for candidate in election.candidates
            ],
            'status': election.status,
            'transitions': election_service.next_election_transitions(election),
            'voting_begins_epoch_millis': election.voting_begins_epoch_millis,
            'voting_ends_epoch_millis': election.voting_ends_epoch_millis,
        }
        return jsonify(result)


@election_api.route('/election', methods=['POST'])
@requires_auth(admin=True)
def add_election(ctx: AuthContext):
    election = Election(
        name=request.json['name'],
        number_winners=int(request.json['number_winners']),
    )
    ctx.session.add(election)
    candidates = request.json['candidate_list']
    members = ctx.session.query(Member).filter(Member.email_address.in_(candidates)).all()
    for member in members:
        candidate = Candidate()
        candidate.election = election
        candidate.name = member.name
        ctx.session.add(candidate)
    ctx.session.commit()
    return jsonify({'status': 'success'})


@election_api.route('/election/<int:election_id>/state/<state_transition>', methods=['PUT'])
@requires_auth(admin=True)
def change_election_state(ctx: AuthContext,
                          election_id: int,
                          state_transition: str):
    (result, election) = election_service.transition_election_as(
        ctx.session,
        state_transition,
        election_id)

    if not result:
        if election is None:
            return NotFound('Election id={} does not exist'.format(election_id))
        else:
            return BadRequest('Invalid state transition "{}"'.format(state_transition))

    return jsonify({
        'status': 'success',
        'election_status': election.status,
        'transitions': election_service.next_election_transitions(election),
    })


@election_api.route('/election/eligible/list', methods=['GET'])
@requires_auth(admin=True)
def get_eligible(ctx: AuthContext):
    election_id = request.args['election_id']
    if election_service.find_election_by_id(ctx.session, election_id) is None:
        return NotFound('Election id={} does not exist'.format(election_id))

    eligibles = ctx.session.query(EligibleVoter) \
        .filter_by(election_id=election_id).options(joinedload(EligibleVoter.member)).all()
    return jsonify({
        eligible.member_id: {
            'name': eligible.member.name,
            'email_address': eligible.member.email_address
        } for eligible in eligibles
    })


@election_api.route('/election/<int:election_id>/vote/<int:ballot_key>', methods=['GET'])
@requires_auth(admin=False)
def get_vote(ctx: AuthContext, election_id: int, ballot_key: int):
    vote = ctx.session.query(Vote) \
        .filter(Vote.election_id == election_id, Vote.vote_key == ballot_key) \
        .one_or_none()
    if not vote:
        return NotFound('Ballot with vote_key={} does not exist for election_id={}'.format(
            ballot_key,
            election_id))
    else:
        rankings = [ranking.candidate_id for ranking in vote.ranking]
        return jsonify({
            'election_id': election_id,
            'ballot_key': ballot_key,
            'rankings': rankings
        })


@election_api.route('/ballot/issue', methods=['POST'])
@requires_auth(admin=True)
def issue_ballot(ctx: AuthContext):
    election_id = request.json.get('election_id')
    if election_id is None:
        return BadRequest('Missing param "election_id"')

    member_id = request.json.get('member_id')
    if member_id is None:
        return BadRequest('Missing param "member_id"')

    eligible = ctx.session.query(EligibleVoter). \
        filter_by(member_id=member_id, election_id=election_id).with_for_update().one_or_none()
    if not eligible:
        return BadRequest('Voter is not eligible for this election.')
    if eligible.voted:
        return BadRequest('Voter has either already voted or received a paper ballot for this '
                          'election.')
    eligible.voted = True
    ctx.session.commit()
    return jsonify({'status': 'success'})


@election_api.route('/ballot/claim', methods=['POST'])
@requires_auth(admin=True)
def add_paper_ballots(ctx: AuthContext):
    election_id = request.json.get('election_id')
    if election_id is None:
        return BadRequest('Missing param "election_id"')

    number_ballots = request.json.get('number_ballots')
    if number_ballots is None:
        return BadRequest('Missing param "number_ballots"')

    ballot_keys = []
    for i in range(0, number_ballots):
        vote, _ = create_vote(ctx.session, election_id, 5)
        ballot_keys.append(vote.vote_key)
    return jsonify(ballot_keys)


@election_api.route('/vote/paper', methods=['POST'])
@requires_auth(admin=True)
def submit_paper_vote(ctx: AuthContext):
    election_id = request.json['election_id']
    election = election_service.find_election_by_id(ctx.session, election_id)
    if election is None:
        return NotFound('Election id={} does not exist'.format(election_id))
    if election.status == 'final':
        return BadRequest('You may not submit more votes after an election has been marked final')
    vote_key = request.json['ballot_key']
    vote = ctx.session.query(Vote).filter_by(
        election_id=election_id,
        vote_key=vote_key,
    ).with_for_update().one_or_none()

    if not vote:
        return NotFound(
            f'Ballot with vote_key={vote_key} for election_id={election_id} not claimed'
        )

    override = request.json.get('override', False)
    rankings = request.json.get('rankings', [])
    if vote.ranking and not override:
        existing_rankings = [ranking.candidate_id for ranking in vote.ranking]
        if rankings == existing_rankings:
            return jsonify({'status': 'match'})
        else:
            return jsonify({'status': 'mismatch'})
    if override:
        for rank in vote.ranking:
            ctx.session.delete(rank)
    for rank, candidate_id in enumerate(rankings):
        ranking = Ranking(rank=rank, candidate_id=candidate_id)
        vote.ranking.append(ranking)
    ctx.session.add(vote)
    ctx.session.commit()
    return jsonify({'status': 'new'})


@election_api.route('/vote', methods=['POST'])
@requires_auth()
def submit_vote(ctx: AuthContext):
    election_id = request.json.get('election_id')
    if election_id is None:
        return BadRequest('Missing param "election_id"')

    rankings = request.json.get('rankings', [])
    maybe_vote_key = handle_vote(requester=ctx.requester, session=ctx.session,
                                 election_id=election_id, rankings=rankings)
    if type(maybe_vote_key) is not VotingError:
        return jsonify({'ballot_id': maybe_vote_key})

    if maybe_vote_key == VotingError.ALREADY_VOTED:
        return BadRequest('You have already voted in this election')
    if maybe_vote_key == VotingError.INELIGIBLE:
        return BadRequest('Not eligible to vote in this election')
    if maybe_vote_key == VotingError.TOO_LATE:
        return BadRequest('You may not submit a vote after the polls have closed')
    if maybe_vote_key == VotingError.TOO_EARLY:
        return BadRequest('Voting is not yet open')
    if maybe_vote_key == VotingError.UNKNOWN_ELECTION:
        return NotFound('Election id={} does not exist'.format(election_id))
    if maybe_vote_key == VotingError.VOTE_KEY_RESERVATION_FAILURE:
        return ServerError('Could not reserve a random vote key. Please try again.')


@election_api.route('/election/voter', methods=['POST'])
@requires_auth(admin=True)
def add_voter(ctx: AuthContext):
    member_id = request.json.get('member_id', ctx.requester.id)
    election_id = request.json.get('election_id')
    if election_id is None:
        return BadRequest('Missing param "election_id"')

    existing_voter = ctx.session.query(EligibleVoter)\
        .filter_by(member_id=member_id, election_id=election_id).one_or_none()

    if existing_voter is None:
        eligible_voter = EligibleVoter(member_id=member_id, election_id=election_id)
        ctx.session.add(eligible_voter)
        ctx.session.commit()

    return jsonify({'status': 'success'})


@election_api.route('/election/count', methods=['GET'])
@requires_auth(admin=True)
def election_count(ctx: AuthContext):
    election_id = request.args['id']
    election = election_service.find_election_by_id(ctx.session, election_id)
    if election is None:
        return NotFound('Election id={} does not exist'.format(election_id))

    stv = hold_election(election)
    winners = [ctx.session.query(Candidate).get(cid).name for cid in stv.winners]
    round_information = {}
    for round_number, round in enumerate(stv.previous_rounds):
        candidate_information = {}
        for cid, vote_info in round.items():
            candidate_name = ctx.session.query(Candidate).get(cid).name
            candidate_information[candidate_name] = vote_info
        round_information[round_number + 1] = candidate_information

    return jsonify({
        'ballot_count': len(stv.votes),
        'quota': stv.quota,
        'winners': winners,
        'round_information': round_information
    })


def hold_election(election: Election):
    votes = [
        [v.candidate_id for v in vote.ranking]
        for vote in election.votes if vote.ranking
    ]
    candidate_ids = [c.id for c in election.candidates]
    logging.info(
        "CANDIDATE_IDS: {}, VOTES: {}, NUM WINNERS: {}".format(
            candidate_ids, votes, election.number_winners
        )
    )
    stv = STVElection(candidate_ids, election.number_winners, votes, election.id)
    stv.hold_election()
    return stv


def create_vote(session: Session, election_id: int, digits: int):
    i = 0
    rolled_back = False
    while i < 5:
        try:
            a = random.randint(10 ** (digits - 1), 10 ** digits - 1)
            v = Vote(vote_key=a, election_id=election_id)
            session.add(v)
            session.commit()
            return v, rolled_back
        except IntegrityError:
            print('Had to retry')
            i += 1
            session.rollback()
            rolled_back = True
    raise Exception('Failing to find a random key in five tries. Think something is wrong.')
