from typing import List, Optional  # NOQA

from flask import Blueprint, jsonify, request

from membership.database.models import Attendee, Meeting, Member
from membership.models import AuthContext
from membership.services import ValidationError, MeetingService
from membership.web.auth import requires_auth
from membership.web.util import BadRequest, NotFound

meeting_api = Blueprint('meeting_api', __name__)
meeting_service = MeetingService()


def meeting_to_dict(meeting: Meeting):
    return {
        'id': meeting.id,
        'name': meeting.name,
        'committee_id': meeting.committee_id,
        'code': meeting.short_id,
        'landing_url': meeting.landing_url,
        'start_time': meeting.start_time,
        'end_time': meeting.end_time,
    }


@meeting_api.route('/meeting', methods=['POST'])
@requires_auth(admin=True)
def create_meeting(ctx: AuthContext):
    try:
        name = request.json['name']
        committee_id = request.json.get('committee_id')
        meeting = meeting_service.add_meeting(name, committee_id, ctx.session)
        meeting = meeting_service.set_meeting_fields(meeting, request.json, ctx.session)
    except ValidationError as e:
        ctx.session.rollback()
        return BadRequest(e.message)

    return jsonify({
        'status': 'success',
        'meeting': meeting_to_dict(meeting),
    })


@meeting_api.route('/meeting/list', methods=['GET'])
@requires_auth(admin=False)
def get_meetings(ctx: AuthContext):
    meetings: List[Meeting] = ctx.session.query(Meeting).order_by(Meeting.id.desc()).all()
    result = [meeting_to_dict(m) for m in meetings]
    return jsonify(result)


@meeting_api.route('/meeting', methods=['PATCH'])
@requires_auth(admin=True)
def update_meeting(ctx: AuthContext):
    # TODO: Do better generic model validation
    meeting_id = str(request.json['meeting_id'])
    if not meeting_id:
        return BadRequest('Missing "meeting_id"')

    meeting = meeting_service.find_meeting_by_id(meeting_id, ctx.session)
    if not meeting:
        return NotFound('Could not find meeting with id={}'.format(meeting_id))

    try:
        meeting_service.set_meeting_fields(meeting, request.json, ctx.session)
    except ValidationError as e:
        ctx.session.rollback()
        return BadRequest(e.message)

    return jsonify({
        'status': 'success',
        'meeting': meeting_to_dict(meeting),
    })


@meeting_api.route('/meeting/attend', methods=['POST'])
@requires_auth(admin=False)
def attend_meeting(ctx: AuthContext):
    try:
        short_id: int = int(request.json['meeting_short_id'])
    except ValueError as e:
        return BadRequest("Cannot parse 'meeting_short_id' as int: {}".format(str(e)))
    try:
        meeting: Optional[Meeting] = meeting_service.attend_meeting(
            ctx.requester.id,
            short_id,
            ctx.session,
        )
        if meeting is None:
            return NotFound('Meeting with short_id={} does not exist'.format(short_id))
        else:
            return jsonify({'status': 'success', 'landing_url': meeting.landing_url})
    except ValidationError as e:
        return BadRequest(e.message)


@meeting_api.route('/meetings/<meeting_id>', methods=['GET'])
@requires_auth(admin=False)
def get_meeting(ctx: AuthContext, meeting_id: int):
    meeting = ctx.session.query(Meeting).filter_by(id=meeting_id).one_or_none()
    if not meeting:
        return NotFound('Meeting id {} does not exist'.format(meeting_id))
    return jsonify(meeting_to_dict(meeting))


@meeting_api.route('/meetings/<meeting_id>/attendees', methods=['GET'])
@requires_auth(admin=True)
def get_meeting_attendees(ctx: AuthContext, meeting_id: int):
    meeting = ctx.session.query(Meeting).filter_by(id=meeting_id).one_or_none()
    if not meeting:
        return NotFound('Meeting id {} does not exist'.format(meeting_id))

    result = [
        {
            'id': attendee.member_id,
            'name': attendee.member.name,
        } for attendee in meeting.attendees
    ]

    return jsonify(result)


@meeting_api.route('/meetings/<meeting_id>/attendees/<member_id>', methods=['DELETE'])
@requires_auth(admin=True)
def remove_meeting_attendee(ctx: AuthContext, meeting_id: int, member_id: int):
    meeting = ctx.session.query(Meeting).filter_by(id=meeting_id).one_or_none()
    if not meeting:
        return NotFound('Meeting id {} does not exist'.format(meeting_id))

    attendees = ctx.session.query(Attendee).filter_by(
        meeting_id=meeting_id,
        member_id=member_id,
    ).all()
    for attendee in attendees:
        ctx.session.delete(attendee)
    ctx.session.commit()

    return jsonify({'status': 'success'})


@meeting_api.route('/meetings/<meeting_id>/attendee', methods=['POST'])
@requires_auth(admin=False)
def add_attendee_from_kiosk(ctx: AuthContext, meeting_id: int):
    meeting = ctx.session.query(Meeting).filter_by(id=meeting_id).one_or_none()
    if not meeting:
        return NotFound('Meeting id {} does not exist'.format(meeting_id))
    email_address = request.json['email_address']
    if not email_address:
        return BadRequest('You must supply an email address to check in')
    member = ctx.session.query(Member).filter_by(email_address=email_address).one_or_none()
    if not member:
        member = Member()
        member.first_name = request.json['first_name']
        member.last_name = request.json['last_name']
        member.email_address = email_address
        ctx.session.commit()

    if ctx.session.query(Attendee) \
            .filter_by(meeting_id=meeting.id, member_id=member.id).count() == 0:
        a = Attendee()
        a.meeting = meeting
        a.member = member
        ctx.session.add(a)
        ctx.session.commit()
    return jsonify({'status': 'success'})
