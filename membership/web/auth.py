import random
import string
from datetime import datetime, timedelta
from functools import wraps

import jwt
import requests
from flask import request, Response, jsonify

from config import JWT_SECRET, JWT_CLIENT_ID, ADMIN_CLIENT_ID, ADMIN_CLIENT_SECRET, \
    AUTH_CONNECTION, AUTH_URL, USE_AUTH, NO_AUTH_EMAIL
from config import PORTAL_URL
from membership.database.base import Session
from membership.database.models import Member
from membership.models import AuthContext

PASSWORD_CHARS = string.ascii_letters + string.digits


def deny(reason: str= '') -> Response:
    """Sends a 401 response that enables basic auth"""
    response = jsonify({
        'status': 'error',
        'err': 'Could not verify your access level for that URL.\n'
               'You have to login with proper credentials and' + reason
    })
    response.status_code = 401
    return response


def requires_auth(admin: bool = False):
    """ This defines a decorator which when added to a route function in flask requires authorization to
    view the route.
    """
    def decorator(f):
        @wraps(f)
        def decorated(*args, **kwargs):
            if USE_AUTH:
                auth = request.headers.get('authorization')
                if not auth:
                    return deny('Authorization not found.')
                token = auth.split()[1]
                try:
                    token = jwt.decode(token, JWT_SECRET, audience=JWT_CLIENT_ID)
                except Exception as e:
                    return deny(str(e))
                email = token.get('email')
            else:
                email = NO_AUTH_EMAIL
            session = Session()
            try:
                member = session.query(Member).filter_by(email_address=email).one()
                ctx = AuthContext(member, session)
                if not admin or ctx.az.has_role('admin'):
                    kwargs['ctx'] = ctx
                    return f(*args, **kwargs)
                return deny('not enough access')
            finally:
                session.close()

        return decorated
    return decorator


current_token = {}


def get_auth0_token() -> str:
    if not current_token or datetime.now() > current_token['expiry']:
        current_token.update(generate_auth0_token())
    return current_token['token']


def generate_auth0_token() -> dict:
    payload = {'grant_type': "client_credentials",
               'client_id': ADMIN_CLIENT_ID,
               'client_secret': ADMIN_CLIENT_SECRET,
               'audience': AUTH_URL + 'api/v2/'}
    response = requests.post(AUTH_URL + 'oauth/token', json=payload).json()
    return {'token': response['access_token'],
            'expiry': datetime.now() + timedelta(seconds=response['expires_in'])}


def create_auth0_user(email: str) -> str:
    if not USE_AUTH:
        return PORTAL_URL
    # create the user
    payload = {
        'connection': AUTH_CONNECTION,
        'email': email,
        'password': ''.join(random.SystemRandom().choice(PASSWORD_CHARS) for _ in range(12)),
        'user_metadata': {},
        'email_verified': False,
        'verify_email': False
    }
    headers = {'Authorization': 'Bearer ' + get_auth0_token()}
    r = requests.post(AUTH_URL + 'api/v2/users', json=payload, headers=headers)
    if r.status_code > 299:
        msg = 'Failed to create user. ' \
              'Received status={} content={}'.format(r.status_code, r.content)
        raise Exception(msg)
    user_id = r.json()['user_id']

    # get a password change URL
    payload = {
        'result_url': PORTAL_URL,
        'user_id': user_id
    }
    r = requests.post(AUTH_URL + 'api/v2/tickets/password-change', json=payload, headers=headers)
    if r.status_code > 299:
        msg = 'Failed to get password url. ' \
              'Received status={} content={}'.format(r.status_code, r.content)
        raise Exception(msg)
    reset_url = r.json()['ticket']

    # get email verification link
    payload = {
        'result_url': reset_url,
        'user_id': user_id
    }
    r = requests.post(AUTH_URL + 'api/v2/tickets/email-verification', json=payload, headers=headers)
    if r.status_code > 299:
        msg = 'Failed to get verify url. ' \
              'Received status={} content={}'.format(r.status_code, r.content)
        raise Exception(msg)
    validate_url = r.json()['ticket']
    return validate_url
