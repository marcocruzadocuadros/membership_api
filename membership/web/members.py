import logging

from flask import Blueprint, jsonify, request
from sqlalchemy.exc import IntegrityError

from config import USE_EMAIL
from membership.database.base import Session
from membership.database.models import Attendee, Committee, Meeting, Member, Role
from membership.models import AuthContext
from membership.repos import MeetingRepo
from membership.schemas.rest.members import \
    format_member_basics, format_member_full, format_member_query_result, \
    format_eligible_member_list
from membership.services import AttendeeService, MemberService
from membership.util.email import send_welcome_email
from membership.web.auth import create_auth0_user, requires_auth
from membership.web.util import BadRequest, ServerError

member_api = Blueprint('member_api', __name__)
meeting_repository = MeetingRepo(Meeting)
attendee_service = AttendeeService(
    meetings=meeting_repository
)
member_service = MemberService(attendee_service)


@member_api.route('/member/list', methods=['GET'])
@requires_auth(admin=True)
def get_members(ctx: AuthContext):
    if request.args.get('page_size', None):
        page_size = int(request.args.get('page_size', None))
        cursor = request.args.get('cursor', None)
        member_query_result = member_service.query(cursor=cursor,
                                                   page_size=page_size,
                                                   query_str=None,
                                                   session=ctx.session)
        return jsonify(format_member_query_result(member_query_result))
    else:
        # TODO(ageiduschek): Kill this code path once we support
        # pagination results on the frontend
        return jsonify(get_members_list_deprecated(ctx.session))


@member_api.route('/member/search', methods=['GET'])
@requires_auth(admin=True)
def search_members(ctx: AuthContext):
    page_size = int(request.args.get('page_size', 10))
    query = request.args.get('query', None)
    cursor = request.args.get('cursor', None)
    member_query_result = member_service.query(cursor=cursor,
                                               page_size=page_size,
                                               query_str=query,
                                               session=ctx.session)
    return jsonify(format_member_query_result(member_query_result))


@member_api.route('/member', methods=['GET'])
@requires_auth(admin=False)
def get_member(ctx: AuthContext):
    member = format_member_basics(ctx.requester, ctx.az)
    return jsonify(member)


# TODO(ageiduschek): Kill this code path once we support
# pagination results on the frontend
def get_members_list_deprecated(session: Session):
    members_result = member_service.all(session)
    eligible_members = attendee_service.members_as_eligible(session, members_result.members)
    return format_eligible_member_list(eligible_members)


@member_api.route('/member/details', methods=['GET'])
@requires_auth(admin=False)
def get_member_details(ctx: AuthContext):
    member = format_member_full(ctx.requester, ctx.az)
    return jsonify(member)


@member_api.route('/admin/member/details', methods=['GET'])
@requires_auth(admin=True)
def get_member_info(ctx: AuthContext):
    other_member = ctx.session.query(Member).get(request.args['member_id'])
    return jsonify(format_member_full(other_member, ctx.az))


@member_api.route('/member', methods=['POST'])
@requires_auth(admin=True)
def add_member(ctx: AuthContext):
    email_address = request.json['email_address'].strip() or None
    first_name = request.json['first_name'].strip() or None
    last_name = request.json['last_name'].strip() or None
    if not email_address and not (first_name and last_name):
        return BadRequest('You must supply either an email or a full name to add a member')

    # TODO: Move to member service
    member = Member(first_name=first_name, last_name=last_name, email_address=email_address)
    ctx.session.add(member)
    try:
        ctx.session.flush()
    except IntegrityError:
        ctx.session.rollback()
        return BadRequest(f"Member with email already exists: {email_address}")

    try:
        verify_url = create_auth0_user(member.email_address)
    except Exception as e:
        ctx.session.delete(member)
        logging.error(f"Could not create Auth0 user for {member.email_address}: {e}")
        return ServerError(f"Error creating user on Auth0")

    email_sent = False
    if USE_EMAIL:
        ctx.session.refresh(member)
        try:
            send_welcome_email(member.email_address, member.first_name, verify_url)
            email_sent = True
        except Exception as e:
            logging.error(f"Could not send welcome email to member.id={member.id}: {e}")

    return jsonify(
        {
            'status': 'success',
            'data': {
                'member': format_member_basics(member, ctx.az),
                'verify_url': verify_url,
                'email_sent': email_sent,
            }
        }
    )


@member_api.route('/committee/list', methods=['GET'])
@requires_auth(admin=False)
def get_committees(ctx: AuthContext):
    committees = ctx.session.query(Committee).all()
    result = {c.id: c.name for c in committees}
    return jsonify(result)


@member_api.route('/committee', methods=['POST'])
@requires_auth(admin=True)
def add_committee(ctx: AuthContext):
    committee = Committee(name=request.json['name'])
    ctx.session.add(committee)
    admins = request.json['admin_list']
    members = ctx.session.query(Member).filter(Member.email_address.in_(admins)).all()
    for member in members:
        role = Role(role='admin')
        role.committee = committee
        role.member = member
        ctx.session.add(role)
    ctx.session.commit()
    return jsonify({'status': 'success'})


@member_api.route('/admin', methods=['POST'])
@requires_auth(admin=True)
def make_admin(ctx: AuthContext):
    member = ctx.session.query(Member).filter_by(email_address=request.json['email_address']).one()
    committee_id = request.json['committee'] if request.json['committee'] != '0' else None
    role = Role(member_id=member.id, role='admin', committee_id=committee_id)
    ctx.session.add(role)
    ctx.session.commit()
    return jsonify({'status': 'success'})


@member_api.route('/member/role', methods=['POST'])
@requires_auth(admin=True)
def add_role(ctx: AuthContext):
    member_id = request.json.get('member_id', ctx.requester.id)
    committee_id = request.json['committee_id'] if request.json['committee_id'] != '0' else None

    existing_role = ctx.session.query(Role) \
        .filter_by(member_id=member_id, role=request.json['role'], committee_id=committee_id)\
        .first()
    if existing_role is not None:
        return BadRequest('Member already has this role')

    role = Role(member_id=member_id, role=request.json['role'], committee_id=committee_id)
    ctx.session.add(role)
    ctx.session.commit()
    return jsonify({'status': 'success'})


@member_api.route('/member/attendee', methods=['POST'])
@requires_auth(admin=True)
def add_attendee(ctx: AuthContext):
    meeting_id = request.json['meeting_id']
    member_id = request.json.get('member_id', ctx.requester.id)
    if ctx.session.query(Attendee) \
            .filter_by(meeting_id=meeting_id, member_id=member_id).count() == 0:
        attend = Attendee(member_id=member_id, meeting_id=meeting_id)
        ctx.session.add(attend)
        ctx.session.commit()
    return jsonify({'status': 'success'})
