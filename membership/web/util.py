import json
import logging
from datetime import date, datetime
from decimal import Decimal
from typing import Optional

from flask import Response
from flask.json import JSONEncoder

from membership.schemas import Json

logger = logging.getLogger(__name__)


class Ok(Response):
    def __init__(self, data: Optional[Json] = None) -> None:
        payload = {'status': 'success'}
        if data:
            payload['data'] = data
        super(Ok, self).__init__(
            json.dumps(payload), status=200, mimetype='application/json')


class NotFound(Response):
    def __init__(self, err: str) -> None:
        payload = {'status': 'failed', 'err': err}
        super(NotFound, self).__init__(
            json.dumps(payload), status=404, mimetype='application/json')


class BadRequest(Response):
    def __init__(self, err: str) -> None:
        payload = {'status': 'failed', 'err': err}
        super(BadRequest, self).__init__(
            json.dumps(payload), status=400, mimetype='application/json')


class ServerError(Response):
    def __init__(self, err: str) -> None:
        payload = {'status': 'failed', 'err': err}
        super(ServerError, self).__init__(
            json.dumps(payload), status=500, mimetype='application/json')


class CustomEncoder(JSONEncoder):
    """ Custom encoder class converts Decimals to strings and datetime objects into ISO
    formatted strings. """

    def default(self, obj):
        if isinstance(obj, Decimal):
            return str(obj)
        if isinstance(obj, datetime):
            return obj.isoformat()
        if isinstance(obj, date):
            return obj.isoformat()
        return JSONEncoder.default(self, obj)
