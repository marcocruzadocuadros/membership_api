from membership.models import EligibleMember
from typing import List


class MemberQueryResult:

    def __init__(self,
                 members: List[EligibleMember],
                 cursor: str,
                 has_more: bool):
        self.members = members
        self.cursor = cursor
        self.has_more = has_more
