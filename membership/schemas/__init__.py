from typing import Dict, List, Union


Json = Union[str, float, int, List['JsonType'], Dict[str, 'JsonType']]
JsonObj = Dict[str, Json]
