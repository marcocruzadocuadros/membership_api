import json
from datetime import datetime

from werkzeug.test import Client
from werkzeug.wrappers import BaseResponse

from config import PORTAL_URL, SUPER_USER_EMAIL
from membership.database.base import engine, metadata, Session
from membership.database.models import Attendee, Committee, Meeting, Member, Role
from membership.web.base_app import app
from tests.flask_utils import get_json, post_json


class TestWebMembers:
    def setup(self):
        metadata.create_all(engine)
        self.app = Client(app, BaseResponse)
        self.app = app.test_client()
        self.app.testing = True

        # set up for auth
        session = Session()
        m = Member()
        m.email_address = SUPER_USER_EMAIL
        session.add(m)
        role = Role()
        role.member = m
        role.role = 'admin'
        session.add(role)

        meeting1 = Meeting(
            id=1,
            short_id=1,
            name='Meeting 1',
            start_time=datetime(2017, 1, 1, 0),
            end_time=datetime(2017, 1, 1, 1),
        )
        meeting2 = Meeting(
            id=2,
            short_id=2,
            name='Meeting 2',
            start_time=datetime(2017, 2, 1, 0),
            end_time=datetime(2017, 2, 1, 1),
        )
        meeting3 = Meeting(
            id=3,
            short_id=3,
            name='Meeting 3',
            start_time=datetime(2017, 3, 1, 0),
            end_time=datetime(2017, 3, 1, 1),
        )
        meeting4 = Meeting(
            id=4,
            short_id=None,
            name='Meeting 4',
            start_time=datetime(2017, 4, 1, 0),
            end_time=datetime(2017, 4, 1, 1),
        )

        attendee2 = Attendee(
            meeting=meeting2,
            member=m
        )
        attendee3 = Attendee(
            meeting=meeting3,
            member=m
        )
        session.add_all([meeting1, attendee2, attendee3, meeting4])

        session.commit()
        session.close()

    def teardown(self):
        metadata.drop_all(engine)

    def test_member_list_status_code(self):
        response = self.app.get('/member/list', content_type='application/json')
        result = response.status_code
        expected = 200

        assert result == expected

    def test_member_list_deprecated(self):
        response = self.app.get('/member/list', content_type='application/json')
        result = json.loads(response.data)
        expected = [
            {
                'id': 1,
                'name': '',
                'email': SUPER_USER_EMAIL,
                'eligibility': {
                    'is_eligible': True,
                    'message': 'eligible (Feb, Mar)',
                },
            },
        ]

        assert result == expected

    def test_member_list(self):
        params = {
            'page_size': 1,
            'cursor': None,
        }
        response = self.app.get('/member/list', content_type='application/json',
                                query_string=params)
        result = json.loads(response.data)
        expected = {
            'members': [
                {
                    'id': 1,
                    'name': '',
                    'email': SUPER_USER_EMAIL,
                    'eligibility': {
                        'is_eligible': True,
                        'message': 'eligible (Feb, Mar)',
                    },
                }],
            'has_more': False,
            'cursor': '1',
        }

        assert result == expected

    def do_search_test(self,
                       params,
                       should_match_user,
                       expected_has_more,
                       expected_cursor):
        response = self.app.get('/member/search', content_type='application/json',
                                query_string=params)

        response_code = response.status_code
        assert response_code == 200

        result = json.loads(response.data)

        members = []

        if should_match_user:
            members += [
                {
                    'id': 1,
                    'name': '',
                    'email': SUPER_USER_EMAIL,
                    'eligibility': {
                        'is_eligible': True,
                        'message': 'eligible (Feb, Mar)',
                    },
                }]
        expected = {
            'members': members,
            'has_more': expected_has_more,
            'cursor': expected_cursor,
        }

        assert result == expected

    def test_member_search_null_cursor(self):
        assert '@' in SUPER_USER_EMAIL
        query_str = SUPER_USER_EMAIL.split('@', 1)[0]  # query for part of email before @
        params = {
            'query': query_str,
            'page_size': 1,
            'cursor': None,
        }

        self.do_search_test(params=params,
                            should_match_user=True,
                            expected_has_more=False,
                            expected_cursor='1')

    def test_member_search_nonnull_cursor(self):
        assert '@' in SUPER_USER_EMAIL
        query_str = SUPER_USER_EMAIL.split('@', 1)[0]  # query for part of email before @
        params = {
            'query': query_str,
            'page_size': 1,
            'cursor': '1',
        }

        self.do_search_test(params=params,
                            should_match_user=False,
                            expected_has_more=False,
                            expected_cursor='1')

    def test_member(self):
        response = get_json(self.app, '/member')
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == {
            'id': 1,
            'info': {
                'biography': None,
                'first_name': None,
                'last_name': None,
                'email_address': SUPER_USER_EMAIL,
            },
            'roles': [{'committee': 'general', 'role': 'admin'}]
        }

    def test_member_details(self):
        response = get_json(self.app, '/member/details')
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == {
            'id': 1,
            'info': {
                'biography': None,
                'first_name': None,
                'last_name': None,
                'email_address': SUPER_USER_EMAIL,
            },
            'roles': [{'committee': 'general', 'role': 'admin'}],
            'meetings': ['Meeting 2', 'Meeting 3'],
            'votes': []
        }

    def test_member_info(self):
        response = get_json(self.app, '/admin/member/details?member_id=1')
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == {
            'id': 1,
            'info': {
                'biography': None,
                'first_name': None,
                'last_name': None,
                'email_address': SUPER_USER_EMAIL,
            },
            'roles': [{'committee': 'general', 'role': 'admin'}],
            'meetings': ['Meeting 2', 'Meeting 3'],
            'votes': []
        }

    def test_add_member(self):
        first_name = 'Eugene'
        last_name = 'Debs'
        email_address = 'debs1855@gmail.com'
        payload = {
            'email_address': email_address,
            'first_name': first_name,
            'last_name': last_name,
        }
        response = post_json(self.app, '/member', payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result['status'] == 'success'
        assert result['data'] == {
            'email_sent': False,
            'member': {
                'id': 2,
                'info': {
                    'biography': None,
                    'first_name': first_name,
                    'last_name': last_name,
                    'email_address': email_address,
                },
                'roles': [],
            },
            'verify_url': PORTAL_URL,
        }

        session = Session()
        assert session.query(Member).filter_by(email_address=email_address).count() == 1

    def test_get_committees(self):
        response = get_json(self.app, '/committee/list')
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == {}

    def test_add_committee(self):
        payload = {
            'name': 'Committee on Mock Data',
            'admin_list': [SUPER_USER_EMAIL]
        }
        response = post_json(self.app, '/committee', payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == {'status': 'success'}

        session = Session()
        committee = session.query(Committee).filter_by(name=payload['name']).one_or_none()
        assert committee is not None
        assert session.query(Role).filter_by(committee=committee, member_id=1, role='admin')\
                      .count() == 1

    def test_add_attendee(self):
        payload = {
            'meeting_id': 1,
            'member_id': 1,
        }
        response = post_json(self.app, '/member/attendee', payload=payload)
        assert response.status_code == 200

        session = Session()
        assert session.query(Attendee).filter_by(meeting_id=1, member_id=1).count() == 1

    def test_add_attendee_duplicate(self):
        session = Session()
        assert session.query(Attendee).filter_by(meeting_id=3, member_id=1).count() == 1

        payload = {
            'meeting_id': 3,
            'member_id': 1,
        }
        response = post_json(self.app, '/member/attendee', payload=payload)
        assert response.status_code == 200

        assert session.query(Attendee).filter_by(meeting_id=3, member_id=1).count() == 1

    def test_make_admin(self):
        payload = {
            'email_address': SUPER_USER_EMAIL,
            'committee': 1
        }
        response = post_json(self.app, '/admin', payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == {'status': 'success'}

        session = Session()
        assert session.query(Role).filter_by(member_id=1, committee_id=1, role='admin').count() == 1

    def test_member_role(self):
        payload = {
            'member_id': 1,
            'committee_id': 1,
            'role': 'admin',
        }
        response = post_json(self.app, '/member/role', payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == {'status': 'success'}

    def test_member_role_duplicate(self):
        payload = {
            'member_id': 1,
            'committee_id': None,
            'role': 'admin',
        }
        response = post_json(self.app, '/member/role', payload=payload)
        assert response.status_code == 400
