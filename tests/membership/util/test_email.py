from config import SUPER_USER_EMAIL, SUPER_USER_FIRST_NAME, SUPER_USER_LAST_NAME
from membership.util.email import send_welcome_email


class TestEmail:

    def test_send_welcome_email(self):
        name = SUPER_USER_FIRST_NAME + ' ' + SUPER_USER_LAST_NAME
        result = send_welcome_email(SUPER_USER_EMAIL, name, 'https://dsasftest.org/')
        assert result is None
