from membership.util.vote import STVElection


def test_quota():
    candidates = ['Alice', 'Bob', 'Carol']
    votes = []
    votes.extend([['Carol', 'Bob', 'Alice']] * 20)
    election = STVElection(candidates, 2, votes, 1)

    # With 20 valid votes and 2 seats to fill, the STV quota should be:
    #   floor(20 / (2 + 1)) + 1 = 7.
    assert election.quota == 7


def test_transfer():
    candidates = ['Alice', 'Bob', 'Carol']
    votes = []
    votes.extend([['Carol', 'Bob', 'Alice']] * 12)
    votes.extend([['Alice', 'Carol', 'Bob']] * 5)
    election = STVElection(candidates, 2, votes, 1)
    election.hold_election()

    # After Carol the first 6 ballots with Carol as a first choice are counted, the remaining 6 of
    # those ballots should go to Bob in the second round, defeating Alice.
    assert election.winners == ['Carol', 'Bob']
    assert election.previous_rounds == [
        {
            'Alice': {'total_votes': 5, 'total_transfer_votes': 0},
            'Bob': {'total_votes': 0, 'total_transfer_votes': 0},
            'Carol': {'total_votes': 12, 'total_transfer_votes': 0},
        },
        {
            'Alice': {'total_votes': 5, 'total_transfer_votes': 0},
            'Bob': {'total_votes': 6, 'total_transfer_votes': 6},
        }
    ]


def test_tie_break():
    candidates = ['Alice', 'Bob', 'Carol', 'Doug']
    votes = []
    votes.extend([['Carol', 'Bob', 'Alice', 'Doug']] * 10)
    votes.extend([['Bob', 'Carol', 'Alice', 'Doug']] * 4)
    votes.extend([['Doug', 'Carol', 'Alice', 'Bob']] * 1)
    votes.extend([['Doug', 'Carol', 'Bob', 'Alice']] * 1)
    votes.extend([['Alice', 'Carol', 'Bob', 'Doug']] * 6)
    election = STVElection(candidates, 2, votes, 1)
    election.hold_election()

    # Carol wins in the first round with 10 votes with 2 votes transferring to Bob. In the second
    # round, no candidate has enough votes so Doug is eliminated because he has the fewest votes.
    # From Doug, one vote transfers to each of Alice and Bob. In round 3, Alice and Bob each have
    # 7 votes, which is still not enough. Alice should win the tie break because she had more votes
    # in the first round.
    assert election.winners == ['Carol', 'Alice']
    assert election.previous_rounds == [
        {
            'Alice': {'total_votes': 6, 'total_transfer_votes': 0},
            'Bob': {'total_votes': 4, 'total_transfer_votes': 0},
            'Carol': {'total_votes': 10, 'total_transfer_votes': 0},
            'Doug': {'total_votes': 2, 'total_transfer_votes': 0},
        },
        {
            'Alice': {'total_votes': 6, 'total_transfer_votes': 0},
            'Bob': {'total_votes': 6, 'total_transfer_votes': 2},
            'Doug': {'total_votes': 2, 'total_transfer_votes': 0},
        },
        {
            'Alice': {'total_votes': 7, 'total_transfer_votes': 1},
            'Bob': {'total_votes': 7, 'total_transfer_votes': 3},
        },
        {
            'Alice': {'total_votes': 14, 'total_transfer_votes': 8},
        },
    ]


def test_deterministic_tie_break():
    candidates = ['Alice', 'Bob', 'Carol']
    votes = []
    votes.extend([['Carol', 'Bob', 'Alice']] * 2)
    votes.extend([['Bob', 'Carol', 'Alice']] * 1)
    votes.extend([['Alice', 'Carol', 'Bob']] * 1)

    # When seed is 1, Alice should always win the tie in the second round.
    election1 = STVElection(candidates, 2, votes, 1)
    election1.hold_election()

    assert election1.winners == ['Carol', 'Alice']
    assert election1.previous_rounds == [
        {
            'Alice': {'total_votes': 1, 'total_transfer_votes': 0},
            'Bob': {'total_votes': 1, 'total_transfer_votes': 0},
            'Carol': {'total_votes': 2, 'total_transfer_votes': 0},
        },
        {
            'Alice': {'total_votes': 1, 'total_transfer_votes': 0},
            'Bob': {'total_votes': 1, 'total_transfer_votes': 0},
        },
        {
            'Alice': {'total_votes': 2, 'total_transfer_votes': 1},
        }
    ]

    # When the seed is 5, Bob should always win the tie in second round.
    election2 = STVElection(candidates, 2, votes, 5)
    election2.hold_election()

    assert election2.winners == ['Carol', 'Bob']
    assert election2.previous_rounds == [
        {
            'Alice': {'total_votes': 1, 'total_transfer_votes': 0},
            'Bob': {'total_votes': 1, 'total_transfer_votes': 0},
            'Carol': {'total_votes': 2, 'total_transfer_votes': 0},
        },
        {
            'Alice': {'total_votes': 1, 'total_transfer_votes': 0},
            'Bob': {'total_votes': 1, 'total_transfer_votes': 0},
        },
        {
            'Bob': {'total_votes': 2, 'total_transfer_votes': 1},
        }
    ]
