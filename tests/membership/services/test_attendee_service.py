from datetime import datetime
from typing import List

from membership.database.base import Session
from membership.database.models import Attendee, Member, Meeting
from membership.repos import MeetingRepo
from membership.services import AttendeeService
from overrides import overrides

meeting1 = Meeting(
    id=1,
    name='meeting-1',
    short_id=1,
    start_time=datetime(2017, 1, 1),
    end_time=datetime(2017, 1, 1),
)
meeting2 = Meeting(
    id=2,
    name='meeting-2',
    short_id=2,
    start_time=datetime(2017, 2, 1),
    end_time=datetime(2017, 2, 1),
)


# TODO: use abstract base class with actual repository for interface
class FakeMeetingRepository(MeetingRepo):

    def __init__(self, meetings: List[Meeting]) -> None:
        self._all = meetings
        self._most_recent = None

    @overrides
    def most_recent(self, session: Session, limit: int = 3) -> List[Meeting]:
        if self._most_recent is None:
            def sort_by_start_time(meeting: Meeting) -> datetime:
                return meeting.start_time
            self._most_recent = sorted(self._all, key=sort_by_start_time, reverse=True)
        return self._most_recent


class TestAttendeeService:

    def test_members_as_eligible_with_eligible_members(self):
        expected_member = Member(
            id=1,
            email_address='test+eligible@example.com',
        )
        attendee1 = Attendee(
            meeting=meeting1,
            member=expected_member
        )
        attendee2 = Attendee(
            meeting=meeting2,
            member=expected_member
        )
        expected_member.meetings_attended = [
            attendee1,
            attendee2,
        ]

        meeting_repository = FakeMeetingRepository([meeting1, meeting2])
        service = AttendeeService(
            meetings=meeting_repository,
        )

        result = service.members_as_eligible({}, [expected_member])[0]

        assert result.is_eligible is True
        assert result.message == 'eligible (Jan, Feb)'

    def test_eligibility_message_for_member_with_no_attendance(self):
        expected_member = Member(
            id=1,
            email_address='test+noattendance@example.com',
            meetings_attended=[],
        )
        meeting_repository = FakeMeetingRepository([])
        service = AttendeeService(
            meetings=meeting_repository,
        )

        result = service.members_as_eligible({}, [expected_member])[0]

        assert result.is_eligible is False
        assert result.message == 'not eligible'

    # FIXME: this functionali was moved to the serivce but these tests need to be updated to
    # reflect the API and functionality
    '''
    def test_eligible_when_first_two_of_three_meetings(self):
        attended_meetings = [
            Attendee(meeting_id=1),
            Attendee(meeting_id=2)
        ]
        recent_meetings = [
            Meeting(id=1, start_time=datetime(2017, 1, 1)),
            Meeting(id=2, start_time=datetime(2017, 2, 1)),
            Meeting(id=3, start_time=datetime(2017, 3, 1))
        ]

        r = MeetingRepository(Session())
        result = r.is_eligible(attended_meetings, recent_meetings)
        expected = True

        assert result == expected

    def test_eligible_when_last_two_of_three_meetings(self):
        attended_meetings = [
            Attendee(meeting_id=2),
            Attendee(meeting_id=3),
        ]
        recent_meetings = [
            Meeting(id=1, start_time=datetime(2017, 1, 1)),
            Meeting(id=2, start_time=datetime(2017, 2, 1)),
            Meeting(id=3, start_time=datetime(2017, 3, 1))
        ]

        r = MeetingRepository(Session())
        result = r.is_eligible(attended_meetings, recent_meetings)
        expected = True

        assert result == expected

    def test_eligible_when_first_and_last_of_three_meetings(self):
        attended_meetings = [
            Attendee(meeting_id=1),
            Attendee(meeting_id=3),
        ]
        recent_meetings = [
            Meeting(id=1, start_time=datetime(2017, 1, 1)),
            Meeting(id=2, start_time=datetime(2017, 2, 1)),
            Meeting(id=3, start_time=datetime(2017, 3, 1))
        ]

        r = MeetingRepository(Session())
        result = r.is_eligible(attended_meetings, recent_meetings)
        expected = True

        assert result == expected

    def test_not_eligible_when_attended_one_meeting(self):
        attended_meetings = [
            Attendee(meeting_id=3),
        ]
        recent_meetings = [
            Meeting(id=1, start_time=datetime(2017, 1, 1)),
            Meeting(id=2, start_time=datetime(2017, 2, 1)),
            Meeting(id=3, start_time=datetime(2017, 3, 1))
        ]

        r = MeetingRepository(Session())
        result = r.is_eligible(attended_meetings, recent_meetings)
        expected = False

        assert result == expected

    def test_not_eligible_when_attended_no_meetings(self):
        attended_meetings = []
        recent_meetings = [
            Meeting(id=1, start_time=datetime(2017, 1, 1)),
            Meeting(id=2, start_time=datetime(2017, 2, 1)),
            Meeting(id=3, start_time=datetime(2017, 3, 1))
        ]

        r = MeetingRepository(Session())
        result = r.is_eligible(attended_meetings, recent_meetings)
        expected = False

        assert result == expected

    def test_not_eligible_when_attended_multiple_meetings_too_far_in_the_past(self):
        attended_meetings = [
            Attendee(meeting_id=4),
            Attendee(meeting_id=1),
        ]
        recent_meetings = [
            Meeting(id=1, start_time=datetime(2017, 1, 1)),
            Meeting(id=2, start_time=datetime(2017, 2, 1)),
            Meeting(id=3, start_time=datetime(2017, 3, 1))
        ]

        r = MeetingRepository(Session())
        result = r.is_eligible(attended_meetings, recent_meetings)
        expected = False

        assert result == expected
    '''
